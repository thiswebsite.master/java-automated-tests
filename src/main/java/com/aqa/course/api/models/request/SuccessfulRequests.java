//package com.aqa.course.api.models.request;
//
//import com.aqa.course.api.models.Customer;
//import org.apache.http.HttpStatus;
//
///**
// * SuccessfulRequests contain base API requests with status code verification and data serialization.
// *
// * @author alexpshe
// * @version 1.0
// */
//public class SuccessfulRequests {
//    private final Requests requests;
//
//    public SuccessfulRequests() {
//        this.requests = new Requests();
//    }
//
//    public void createCustomer(Customer customer) {
//        requests.createCustomer(customer)
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_CREATED);
//    }
//
//    public Customer getCustomer(String customerId) {
//        return requests.getCustomer(customerId)
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_OK)
//                .extract().body().as(Customer.class);
//    }
//
//    public void deleteCustomer(String customerId) {
//        requests.deleteCustomer(customerId)
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_ACCEPTED);
//    }
//}
