//package api;
//
//import com.aqa.course.api.models.Customer;
//import org.apache.http.HttpStatus;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.params.ParameterizedTest;
//import org.junit.jupiter.params.provider.Arguments;
//import org.junit.jupiter.params.provider.MethodSource;
//
//import java.util.stream.Stream;
//
//import static io.restassured.RestAssured.given;
//import static org.hamcrest.Matchers.is;
//
///**
// * ApiTest contains API tests for customer CRUD operations.
// *
// * @author alexpshe
// * @version 1.0
// */
//
//public class CustomerTest extends BaseApiTest {
//    @Test
//    public void googleTest() {
//        given()
//                .get("http://google.com/")
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_OK);
//    }
//
//    @Test
//    public void createCustomerAssertByBodeParametersTest() {
//        Customer customer = new Customer("100", "Jane",
//                "Smith", "jane.smith@company.com");
//
//        requests.createCustomer(customer)
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_CREATED);
//        createdCustomers.add(customer);
//
//        requests.getCustomer(customer.getId())
//                .then()
//                .assertThat().statusCode(HttpStatus.SC_OK)
//                .body("id", is("100"))
//                .body("firstName", is("Jane"))
//                .body("lastName", is("Smith"))
//                .body("email", is("jane.smith@company.com"));
//    }
//
//    private static Stream<Arguments> customers() {
//        return Stream.of(
//                Arguments.of(new Customer("100", "Jane", "Smith", "jane.smith@company.com")), // тест кейс 1
//                Arguments.of(new Customer("101", "Kate", "Jone", "kate.jone@company.com")) // тест кейс 2
//        );
//    }
//
//    @ParameterizedTest
//    @MethodSource("customers")
//    public void createCustomerTest(Customer expectedCustomer) {
//        successfulRequests.createCustomer(expectedCustomer);
//        createdCustomers.add(expectedCustomer);
//        Customer actualCustomer = successfulRequests.getCustomer(expectedCustomer.getId());
//
//        softly.assertThat(actualCustomer).isEqualTo(expectedCustomer);
//    }
//
//
//}
