import com.aqa.course.Calculator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * CalculatorTest includes tests for Calculator class.
 *
 * @author Yelena39
 * @version 1.0
 */
public class CalculatorTest {
    private Calculator calculator;
    private SoftAssertions softly;

    @BeforeEach
    public void setupBeforeAllTest() {
        this.calculator = new Calculator();
        this.softly = new SoftAssertions();
    }

    @AfterEach
    public void assertAll() {
        this.softly.assertAll();
    }

    @DisplayName("Positive test for sum")
    @Tag("positive")
    @ParameterizedTest
    @CsvSource({
            "1, 2, 3", // тест кейс 1
            "-10, -20, -30", // тест кейс 2
            "0, -135, -135" // тест кейс 3
    })
    public void sumTestPositive(int firstValue, int secondValue, int expectedResult) {
        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    @Disabled
    @DisplayName("Negative test for sum")
    @Tag("negative")
    @ParameterizedTest
    @CsvSource({
            "1.5, 2, 3.5", // тест кейс 1
            "-1.0, -0, -1.0", // тест кейс 2
            "10000000000000000000000000000000, 30000000000000000000000000000000, 40000000000000000000000000000000"
            // тест кейс 3
    })
    public void sumTestNegative(double firstValue, double secondValue, double expectedResult) {
        int actualResult = calculator.sum((int) firstValue, (int) secondValue);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForSumOfCollectionValuesPositive() {
        return Stream.of(
                Arguments.of(Arrays.asList(1, 3, 4), 8), // тест кейс 1
                Arguments.of(Arrays.asList(1, 3, 4, 8, 72), 88), // тест кейс 2
                Arguments.of(Arrays.asList(-1, 0, 1), 0) // тест кейс 3
        );
    }

    @DisplayName("Positive test for sum of collection")
    @Tag("positive")
    @ParameterizedTest
    @MethodSource("testDataForSumOfCollectionValuesPositive")
    public void sumOfCollectionValuesTestPositive(List<Integer> numbers, double expectedResult) {
        double actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForSumOfCollectionValuesNegative() {
        return Stream.of(
//                Arguments.of(Arrays.asList(-1, 3, 4, 3.5), 10.5), // тест кейс 1
                Arguments.of(Arrays.asList(-1, 0, 1, 55, 1000000, 36, 1000000000), 1001000091) // тест кейс 2
        );
    }

    @DisplayName("Negative test for sum of collection")
    @Tag("negative")
    @ParameterizedTest
    @MethodSource("testDataForSumOfCollectionValuesNegative")
    public void sumOfCollectionValuesTestNegative(List<Integer> numbers, double expectedResult) {
        double actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForMultipleOfCollection() {
        return Stream.of(
                Arguments.of(Arrays.asList(0), 0), // тест кейс 1
                Arguments.of(Arrays.asList(1, 3), 3), // тест кейс 2
                Arguments.of(Arrays.asList(-1, 0), 0), // тест кейс 3
                Arguments.of(Arrays.asList(-1, -103), 103) // тест кейс 4
//                Arguments.of(Arrays.asList(7777, 2289, 789), 1.40454e10), // тест кейс 5
//                Arguments.of(Arrays.asList(3.5, 1), 3.5) // тест кейс 6
        );
    }

    @DisplayName("Test for multiple")
    @Tag("positive")
    @ParameterizedTest
    @MethodSource("testDataForMultipleOfCollection")
    public void multipleOfCollection(List<Integer> numbers, double expectedResult) {
        double actualResult = calculator.multiple(numbers);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    @DisplayName("Positive test for divide")
    @Tag("positive")
    @ParameterizedTest
    @CsvFileSource(resources = "/test-data-divide-positive.csv")
    public void divideTestPositive(int firstValue, int secondValue, double expectedResult) {
        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    @DisplayName("Negative test for divide")
    @Tag("negative")
    @ParameterizedTest
    @CsvFileSource(resources = "/test-data-divide-negative.csv")
    public void divideTestNegative(double firstValue, double secondValue, double expectedResult) {
        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    @Disabled
    @DisplayName("Negative test for divide(division by zero)")
    @Tag("negative")
    @ParameterizedTest
    @CsvFileSource(resources = "/test-data-divide-negative-divideByZero.csv")
    public void divideTestNegativeDivideByZero(double firstValue, double secondValue, double expectedResult) {
        double actualResult = calculator.divide((int) firstValue, (int) secondValue);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForPowPositive() {
        return Stream.of(
                Arguments.of(1, 5, 1), // тест кейс 1
//                Arguments.of(-5, 2, -25), // тест кейс 2
                Arguments.of(3, 0, 1) // тест кейс 3
//                Arguments.of(53, 53, 2.4356848e+91), // тест кейс 4
//                Arguments.of(3, -1, 0.33333333333) // тест кейс 5
        );
    }

    @DisplayName("Positive test for pow")
    @Tag("positive")
    @ParameterizedTest
    @MethodSource("testDataForPowPositive")
    public void powPositive(int base, int power, double expectedResult) {
        double actualResult = calculator.pow(base, power);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForPowNegative() {
        return Stream.of(
                Arguments.of(3.5, 3, 0.28571428571), // тест кейс 1
                Arguments.of(3.5, 3.1, 48.5971653141) // тест кейс 2
        );
    }

    @Disabled
    @DisplayName("Negative test for pow")
    @Tag("negative")
    @ParameterizedTest
    @MethodSource("testDataForPowNegative")
    public void pow(double base, double power, double expectedResult) {
        double actualResult = calculator.pow((int) base, (int) power);

        softly.assertThat(actualResult).as("We expected " + expectedResult + " but was " + actualResult)
                .isEqualTo(expectedResult);
    }

}
